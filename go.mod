module retwitter

go 1.14

require (
	github.com/dghubble/go-twitter v0.0.0-20201011215211-4b180d0cc78d
	github.com/gin-gonic/gin v1.6.3
	github.com/haibeey/doclite v0.0.0-20200921135336-059bd83fe948
	github.com/spf13/viper v1.7.1
	golang.org/x/oauth2 v0.0.0-20201208152858-08078c50e5b5
)
