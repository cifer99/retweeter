package retweet

import (
	"encoding/json"
	"fmt"
	"github.com/dghubble/go-twitter/twitter"
	"github.com/gin-gonic/gin"
	"github.com/haibeey/doclite"
	"github.com/spf13/viper"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"
	"net/http"
	"os"
	"errors"
	"os/signal"
	"syscall"
	"flag"
)


var consumerKey = flag.String("consumerKey", "gopher", "the species we are studying")
var consumerSecret = flag.String("consumerSecret", "gopher", "the species we are studying")


type cred struct {
	ConsumerKey    string
	ConsumerSecret string
}

func init()  {
	flag.Parse()
}

func getClient(c *cred) *twitter.Client {

	config := &clientcredentials.Config{
		ClientID:     c.ConsumerKey,
		ClientSecret: c.ConsumerSecret,
		TokenURL:     "https://api.twitter.com/oauth2/token",
	}
	// http.Client will automatically authorize Requests
	httpClient := config.Client(oauth2.NoContext)

	// Twitter client
	client := twitter.NewClient(httpClient)
	return client
}

func searchTweets(params string) {
	
	getClient()
	search, resp, err := client.Search.Tweets(&twitter.SearchTweetParams{
		Query: params,
	})

}
